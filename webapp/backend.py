import os
import uuid
import filetype
from ultralytics import YOLO
from flask import request, Response, Flask, send_file
from waitress import serve
from PIL import Image
import json
from sqlalchemy import create_engine, Column, Integer, String, Text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import stomp

app = Flask(__name__)

# DB Connection
# engine = create_engine("postgresql://postgres:password@localhost:5432/postgres", echo=True)
engine = create_engine("postgresql://postgres:password@db:5432/postgres", echo=True)
Base = declarative_base()


class Car(Base):
  __tablename__ = 'cars'

  id = Column(Integer, primary_key=True)
  image_path = Column(String)
  description = Column(Text)
  num_of_cars = Column(Integer)


Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)

# RabbitMQ connection
conn = stomp.Connection([('rabbit', 61613)])
# conn = stomp.Connection([('localhost', 61613)])
conn.connect('guest', 'guest', wait=True)


@app.route("/")
def root():
  with open("index.html") as file:
    return file.read()


@app.route("/admin")
def admin():
  with open("admin.html") as file:
    return file.read()


@app.route("/stomp.js")
def stomp_js():
    with open("stomp.js", "r") as file:
        return file.read()


@app.route("/images/<filename>")
def get_image(filename):
  images_folder = os.path.join(os.path.dirname(__file__), "images")
  return send_file(os.path.join(images_folder, filename))


@app.route("/cars", methods=["GET"])
def get_cars():
  session = Session()
  cars = session.query(Car).order_by(Car.id.desc()).all()
  session.close()

  cars_list = []
  for car in cars:
    car_dict = {
      "id": car.id,
      "imagePath": car.image_path,
      "description": car.description,
      "carsDetected": car.num_of_cars
    }
    cars_list.append(car_dict)

  return json.dumps(cars_list)


@app.route("/detect", methods=["POST"])
def detect():
  """
    :return: a JSON array of objects bounding
    boxes in format
    [[x1,y1,x2,y2,object_type,probability],..]
  """
  buf = request.files["image_file"]
  description = request.form["description"]

  filename = save_image(buf)
  boxes = detect_objects_on_image(Image.open(buf.stream))

  save_car(filename, description, len(boxes))

  # Publish message to RabbitMQ topic exchange
  message = {
    "imagePath": filename,
    "description": description,
    "carsDetected": len(boxes)
  }
  conn.send(
    body=json.dumps(message),
    destination='/topic/images'
  )

  return Response(
    json.dumps(boxes),
    mimetype='application/json'
  )


def detect_objects_on_image(buf):
  """
  :param buf: Input image file stream
  :return: Array of bounding boxes in format
  [[x1,y1,x2,y2,object_type,probability],..]
  """
  model = YOLO("yolov8m.pt")
  results = model.predict(buf)
  result = results[0]
  output = []
  for box in result.boxes:
    class_id = box.cls[0].item()
    class_name = result.names[class_id]

    if class_name.lower() == "car":
      x1, y1, x2, y2 = [
        round(x) for x in box.xyxy[0].tolist()
      ]
      prob = round(box.conf[0].item(), 2)
      output.append([
        x1, y1, x2, y2, class_name, prob
      ])
  return output


def save_image(image_file):
  """
  Save the uploaded image file to the "images" folder.
  :param image_file: Uploaded image file
  :return: Filename of the saved image
  """
  images_folder = os.path.join(os.path.dirname(__file__), "images")
  os.makedirs(images_folder, exist_ok=True)

  # Generate a unique filename using UUID and determine the file extension
  filename = str(uuid.uuid4())
  kind = filetype.guess(image_file)
  if kind is not None:
    filename += "." + kind.extension
  else:
    filename += ".png"

  filepath = os.path.join(images_folder, filename)
  image_file.stream.seek(0)
  image_file.save(filepath)
  return filename


def save_car(image_path, description, num_of_cars):
  session = Session()
  car = Car(image_path=image_path, description=description, num_of_cars=num_of_cars)
  session.add(car)
  session.commit()
  session.close()


serve(app, host='0.0.0.0', port=80)
