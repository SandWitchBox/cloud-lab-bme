# CI/CD Environment and Web Service
This README.md file is part of the repository [SandWitchBox/cloud-lab-bme](https://gitlab.com/SandWitchBox/cloud-lab-bme).

This repository is for the homework assignment of creating a CI/CD environment, as part of my university coursework.

## Homework Assignment:
Throughout the semester, everyone is required to set up a CI/CD environment on their own machine and use it to create a web service that fulfills the following functionalities:

- Upload of images with descriptions
- Automatic car detection on uploaded images and displaying the image on the website with the cars framed
- The website operators should be able to subscribe to the site, i.e., receive notifications about all existing and newly uploaded images, including the description of the image and the number of cars detected by the system in the uploaded image.